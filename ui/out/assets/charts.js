am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_dataviz);
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart);
chart.paddingRight = 25;

// Add data
chart.data = [{
  "x": 0,
  "y": 400,
  "color": "#66BD63"
}, {
  "x": 14,
  "y": 500,
  "color": "#006837"
}, {
  "x": 22,
  "y": 550,
  "color": "#9ED67C"
}, {
  "x": 29,
  "y": 930,
  "color": "#B7D7BD"
}, {
  "x": 43,
  "y": 1020,
  "color": "#66BD63"
}];


// Create axes
var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
xAxis.renderer.grid.template.disabled = true;
xAxis.renderer.labels.template.disabled = true;
xAxis.numberFormatter = new am4core.NumberFormatter();
xAxis.numberFormatter.numberFormat = "#,###'km'";
xAxis.min = 0;
xAxis.max = 50;
xAxis.strictMinMax = true;

var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
yAxis.numberFormatter = new am4core.NumberFormatter();
yAxis.numberFormatter.numberFormat = "#,###'m'";
yAxis.min = 400;
yAxis.max = 1400;
yAxis.strictMinMax = true;

// Create series
var series = chart.series.push(new am4charts.LineSeries());
series.dataFields.valueX = "x";
series.dataFields.valueY = "y";
series.name = name;
series.tooltipText = "{valueX}km: [bold]{valueY}m[/]";
series.strokeWidth = 0;
series.fillOpacity = 0.8;
series.propertyFields.fill = "color";

// Create grid/ranges for X axis
for(var i = 0; i < chart.data.length; i++) {
  var value = chart.data[i].x;
  var range = xAxis.axisRanges.create();
  range.value = value;
  range.label.text = value + "km";
}

// Cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.snapToSeries = series;

}); // end am4core.ready()



/************************ */

am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end
  
  // Create chart instance
  var chart = am4core.create("chartdiv2", am4charts.PieChart);
  
  // Let's cut a hole in our Pie chart the size of 40% the radius
  chart.innerRadius = am4core.percent(40);
  
  
  
  // Add and configure Series
  var pieSeries = chart.series.push(new am4charts.PieSeries());
  pieSeries.dataFields.value = "value";
  pieSeries.dataFields.category = "category";
  pieSeries.slices.template.stroke = am4core.color("#fff");
  pieSeries.innerRadius = 10;
  pieSeries.slices.template.fillOpacity = 0.5;
  
  pieSeries.slices.template.propertyFields.disabled = "labelDisabled";
  pieSeries.labels.template.propertyFields.disabled = "labelDisabled";
  pieSeries.ticks.template.propertyFields.disabled = "labelDisabled";
  
  
  // Add data
  pieSeries.data = [{
    "category": "First + Second",
    "value": 60
  }, {
    "category": "Unused",
    "value": 30,
    "labelDisabled":true
  }];
  
  // Disable sliding out of slices
  pieSeries.slices.template.states.getKey("hover").properties.shiftRadius = 0;
  pieSeries.slices.template.states.getKey("hover").properties.scale = 1;
  
  // Add second series
  var pieSeries2 = chart.series.push(new am4charts.PieSeries());
  pieSeries2.dataFields.value = "value";
  pieSeries2.dataFields.category = "category";
  pieSeries2.slices.template.states.getKey("hover").properties.shiftRadius = 0;
  pieSeries2.slices.template.states.getKey("hover").properties.scale = 1;
  pieSeries2.slices.template.propertyFields.fill = "fill";
  
  // Add data
  pieSeries2.data = [{
    "category": "Ropa",
    "value": 30,
    "fill":"#006837"
  }, {
    "category": "Zapatos",
    "value": 30,
    "fill":"#1A9850"
  }, {
    "category": "Adornos",
    "value": 30,
    "fill":"#66BD63"
  },{
    "category": "Hogar",
    "value": 30,
    "fill":"#B7D7BD"
  }];
  
  
  pieSeries.adapter.add("innerRadius", function(innerRadius, target){
    return am4core.percent(40);
  })
  
  pieSeries2.adapter.add("innerRadius", function(innerRadius, target){
    return am4core.percent(60);
  })
  
  pieSeries.adapter.add("radius", function(innerRadius, target){
    return am4core.percent(100);
  })
  
  pieSeries2.adapter.add("radius", function(innerRadius, target){
    return am4core.percent(80);
  })
  
  }); // end am4core.ready()